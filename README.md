# Background

Hibernate is a framework that provides ORM for Java.

# Configuration

The JPA implementation of Hibernate uses the standard configuration file for JPA, namely *persistence.xml*. 
This file must be located on the classpath inside a folder named META-INF. It basically contains three elements
(besides the name of the persistence unit):

* **Provider**: the name of the implementation class provided by the backend and enclosed in the <provider> element. In
the case of Hibernate it is org.hibernate.jpa.HibernatePersistenceProvider that is located in the org.hibernate:hibernate-core artifact.
* **Managed classes**: the name of the classes that are meant to be persisted (these classes must be annotated with @Entity too)
enclosed in the <class> element.
* **Properties**: data necessary to access the database: javax.persistence.jdbc.{driver,url,user,password}


# Criteria Query

Criteria API is part of the JPA 2 specification and is an alternative to another search-and-query 
mechanisms as JPQL[^jpql], HQL[^hql] and SQL.

As stated on [the official documentation](http://docs.jboss.org/hibernate/stable/entitymanager/reference/en/html_single/#querycriteria):

> Criteria queries are a programmatic, type-safe way to express a query. They are type-safe in terms of 
> using interfaces and classes to represent various structural parts of a query such as the query itself, 
> or the select clause, or an order-by, etc.

[^jpql]: Java Persistence Query Language
[^hql]: Hibernate Query Language