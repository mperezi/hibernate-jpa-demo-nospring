import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CriteriaQueryTest {

    @Test
    public void testFindUserWithAgeLessThan() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPA2Tutorial");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Client client1 = new Client();
        client1.setName("Pedrito");
        client1.setAge(20);

        Client client2 = new Client();
        client2.setName("Pablito");
        client2.setAge(33);

        em.persist(client1);
        em.persist(client2);

        em.getTransaction().commit();
        em.close();

        em = emf.createEntityManager();

        CriteriaBuilder cb = emf.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> clientRoot = cq.from(Client.class);
        cq.where(cb.lessThan(clientRoot.get(Client_.age), 30));

        List<Client> clients = em.createQuery(cq).getResultList();
        assertEquals(1, clients.size());

        em.close();
    }
}
