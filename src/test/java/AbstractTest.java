import org.hibernate.LazyInitializationException;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;

public class AbstractTest {

    @Test
    public void testNewClient() {
        EntityManager em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();

        Client client = new Client();
        client.setName("Pepito");

        em.getTransaction().begin();
        em.persist(client);
        em.getTransaction().commit();

        System.out.println(client);

        Client foundClient = em.find(Client.class, client.getId());

        System.out.println(foundClient);

        assertEquals(client.getName(), foundClient.getName());

        em.close();
    }

    @Test(expected = Exception.class)
    public void testNewClientWithTxn() throws Exception {
        EntityManager em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();
        em.getTransaction().begin();
        try {
            Client client = new Client();
            client.setName("Juanito");

            em.persist(client);
            if (true) {
                throw new Exception();
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }

        em.close();
    }

    @Test
    public void testNewClientAndAddBank() {
        EntityManager em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();

        em.getTransaction().begin();

        Client client = new Client();
        client.setName("Jorgito");
        Bank bank = new Bank();
        bank.setName("BBVA");

        em.persist(client);
        em.persist(bank);

        em.getTransaction().commit();

        assertEquals(0, client.getBanks().size());

        em.getTransaction().begin();

        client.addBank(bank);
        em.merge(client);

        em.getTransaction().commit();

        assertEquals(1, client.getBanks().size());

        em.close();
    }

    @Test
    public void testFindUser() {
        EntityManager em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();

        em.getTransaction().begin();

        String name = "Jaimito";
        Client client = new Client();
        client.setName(name);
        Bank bank = new Bank();
        bank.setName("Santander");
        client.addBank(bank);

        em.persist(client);
        em.persist(bank);

        em.getTransaction().commit();
        em.close();

        em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();
        Client foundClient = em
                .createNamedQuery("Client.findByName", Client.class)
                .setParameter("name", name)
                .getSingleResult();

        System.out.println(foundClient);

        assertEquals(name, foundClient.getName());
        assertEquals(1, foundClient.getBanks().size());

        System.out.println(foundClient.getBanks().getClass());

        em.close();
    }

    @Test(expected = LazyInitializationException.class)
    // The LazyInitializationException will be thrown on the getBanks() call.
    // This is not a bug. Once the entity manager is closed, any entity can become unusable.
    public void testFindUser1() throws Exception {
        EntityManager em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();

        em.getTransaction().begin();

        String name = "Oscarcito";
        Client client = new Client();
        client.setName(name);
        Bank bank = new Bank();
        bank.setName("Santander");
        client.addBank(bank);

        em.persist(client);
        em.persist(bank);

        em.getTransaction().commit();
        em.close();

        em = Persistence.createEntityManagerFactory("JPA2Tutorial").createEntityManager();
        Client foundClient = em
                .createNamedQuery("Client.findByName", Client.class)
                .setParameter("name", name)
                .getSingleResult();

        em.close();

        assertEquals(1, foundClient.getBanks().size());
    }
}