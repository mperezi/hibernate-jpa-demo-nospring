import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NamedQuery(name = "Client.findByName", query = "select c from Client c where c.name = :name")
public class Client implements Serializable {
    @Id @GeneratedValue
    private Integer id;
    private String name;
    private Integer age;

    @ManyToMany
    private final Set<Bank> banks = new HashSet<>();

    public void addBank(Bank bank) {
        banks.add(bank);
    }
}
